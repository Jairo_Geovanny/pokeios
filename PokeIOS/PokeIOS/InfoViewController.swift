//
//  InfoViewController.swift
//  PokeIOS
//
//  Created by JAIRO PROAÑO on 21/6/18.
//  Copyright © 2018 MATHCRAP. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage


class InfoViewController: ViewController {
    
    @IBOutlet weak var imagePokemon: UIImageView!
    @IBOutlet weak var namePokemon: UILabel!
    var infoPokemon: Pokemon!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        namePokemon.text = infoPokemon.name
        
        Alamofire.request(infoPokemon.sprites.defaultSprite).responseImage { (response) in
            if let image: UIImage = response.result.value {
                print("Image downloaded: \(image)")
                self.imagePokemon.image = image
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
