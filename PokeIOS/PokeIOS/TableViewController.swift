//
//  TableViewController.swift
//  PokeIOS
//
//  Created by JAIRO PROAÑO on 13/6/18.
//  Copyright © 2018 MATHCRAP. All rights reserved.
//

import UIKit

class TableViewController: ViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var pokemons: UITableView!
    
    
    var infoPokemons: [Pokemon] = []
    
    var pokemonSelected:Pokemon?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let network: Network = Network()
        network.getAllPokemon { (pokemons) in
            self.infoPokemons = pokemons
            self.pokemons.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! InfoViewController
        destination.infoPokemon = pokemonSelected
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return infoPokemons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell()
        cell.textLabel?.text = infoPokemons[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        print("Selected: \(infoPokemons[indexPath.row].name)")
        pokemonSelected = infoPokemons[indexPath.row]
        performSegue(withIdentifier: "changeToInfo", sender: nil)
        return indexPath
    }
    
    
    
    
}
