//
//  Pokemon.swift
//  PokeIOS
//
//  Created by JAIRO PROAÑO on 13/6/18.
//  Copyright © 2018 MATHCRAP. All rights reserved.
//

import Foundation

struct Pokemon: Decodable {
    var name: String
    var weight: Int
    var height: Int
    var sprites: Sprite
}

struct Sprite: Decodable {
    var defaultSprite: String
    
    enum CodingKeys: String, CodingKey {
        case defaultSprite = "front_default"
    }
}
